const accounts = [
  {
    name: 'campaigner',
    address: '0xe2508ea5B6F79481f71c5a858314E921e03A8e3C',
    type: 'campaigner'
  },
  {
    name: 'user A',
    address: '0x3B1F915e20D230Cd6b426EE1D47245429D190481',
    type: 'user'
  },
  {
    name: 'user B',
    address: '0xCe7A3024060c5fDacAef04FE314C503934E765AF',
    type: 'user'
  },
  {
    name: 'TGA',
    address: '0xF1c6FBF8fa360E680C93918cb9C966b103eFba8E',
    type: 'tga'
  },
  {
    name: 'PTT',
    address: '0xB32F056337Ebd224beF348Ee8043590DdCA563A1',
    type: 'producer'
  },
  {
    name: 'SCB 10X',
    address: '0x8c49aa3a2D630FDc55b3caFf573b6f7549280DE9',
    type: 'producer'
  }
]

export default {
  fromAddress (address) {
    const found = accounts.filter(e => e.address === address)
    if (found.length > 0) {
      return found[0]
    }

    return {
      name: 'Unknown User',
      type: 'unknown'
    }
  }
}