const abi = require('./abi.json')
const Web3 = require('web3')
import EventBus from './event-bus'

var SimpleContract = null

if (window.ethereum) {
  window.web3 = new Web3(ethereum);
  (async () => {
    try {
      // Request account access if needed
      ethereum.enable();
      // Acccounts now exposed
      console.log("MetaMask Enabled")

      ethereum.on('accountsChanged', function (accounts) {
        location.reload()
      })
    } catch (error) {
        // User denied account access...
        console.log(error)
    }
  })()
} else if (window.web3) {
  // Legacy dapp browsers...
  window.web3 = new Web3(web3.currentProvider);
  console.log('MetaMask connected')
} else {
  // Non-dapp browsers...
  console.log('MetaMask not found')
}

var SimpleContract = null
try {
  ethereum.request({
      method: 'eth_accounts',
      //method: 'eth_requestAccounts',
  }).then(accounts => {
    if (accounts && accounts.length > 0) {
        web3.eth.defaultAccount = accounts[0]
        EventBus.$emit('account-selected', web3.eth.defaultAccount)
        console.log('account', web3.eth.defaultAccount)
    }
  })
  
} catch (err) {
  console.error('Error on init when getting accounts', err)
}

const contractAddress = '0x94c334b704eE3a54b77f2D54Ac77c920af0BD6F4';
SimpleContract = new web3.eth.Contract(abi, contractAddress);

SimpleContract.events.NewCampaign((error, event) => {
  EventBus.$emit('campaign-create', event.returnValues.campaignId, event.returnValues.name, event.returnValues.status)
})

SimpleContract.events.Transfer((error, event) => {
  if (event.returnValues._from === web3.eth.defaultAccount || event.returnValues._to === web3.eth.defaultAccount) {
    EventBus.$emit('balance-transfer', event.returnValues._from === web3.eth.defaultAccount, event.returnValues._value)
  }
})

SimpleContract.events.Retire((error, event) => {
  if (event.returnValues._from === web3.eth.defaultAccount) {
    EventBus.$emit('coin-retire', event.returnValues._value)
  }
})

export default {
  async getBalance() {
    return new Promise((resolve, reject) => {
      SimpleContract.methods.getBalance(web3.eth.defaultAccount).call((err, result) => {
        if (err) {
            console.log(err)
            resolve(0)
        } else {
            console.log('getBalance', result)
            const balance = parseInt(result).toLocaleString()
            EventBus.$emit('refresh-profile-balance', balance)
            resolve(balance)
        }
      })
    })
  },
  async getBalanceInEth() {
    return new Promise((resolve, reject) => {
      SimpleContract.methods.getBalanceInEth(web3.eth.defaultAccount).call((err, result) => {
        if (err) {
            console.log(err)
            resolve(0)
        } else {
            console.log('getBalanceInEth', result)
            const balance = parseInt(result).toLocaleString()
            EventBus.$emit('refresh-profile-balance-eth', this.accountBalanceEth)
            resolve(balance)
        }
      })
    })
  },
  async sendCoin(to, amount) {
    return new Promise((resolve, reject) => {
      SimpleContract.methods.sendCoin(to, amount).send({from: web3.eth.defaultAccount}, (err, result) => {
        if (err) {
            console.log(err)
            resolve(false)
        } else {
            console.log('sendCoin', to, amount, result)
            // console.log(`https://ropsten.etherscan.io/tx/${result}`);
            resolve(true)
        }
      })
    })
  },
  async retireCoin(amount) {
    return new Promise((resolve, reject) => {
      SimpleContract.methods.retireCoin(amount).send({from: web3.eth.defaultAccount}, (err, result) => {
        if (err) {
            console.log(err)
            resolve(false)
        } else {
            console.log('retireCoin', to, amount, result)
            // console.log(`https://ropsten.etherscan.io/tx/${result}`);
            resolve(true)
        }
      })
    })
  }
}