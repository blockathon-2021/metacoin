#include <WiFi.h>
#include <HTTPClient.h>
#include <M5StickC.h>
 
const char* ssid = "THANARAT-WIFI";
const char* password = "1q2w3e4r";
const int ledPin = 10;

void setup() {
  M5.begin();
  M5.Lcd.printf("Initialize...\n");
  pinMode (ledPin, OUTPUT);
  M5.Lcd.printf("[OK] LED\n");
//  digitalWrite (ledPin, LOW); //off
  Serial.begin(115200);
  delay(4000);
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(3000);
    M5.Lcd.printf("Wifi...\n");
    Serial.println("Connecting WiFi");
  }
    M5.Lcd.printf("[OK] Wifi\n");
 
  Serial.println("Connected to the WiFi network");
    M5.Lcd.printf("System ready!\n");
 
}
 
void loop() {
  M5.update(); // need to call update()
  M5.Lcd.setCursor(0, 0);
  if (M5.BtnA.isPressed()) {
    M5.Lcd.printf("[A] pressed\n");
    led_on();
    iotpush();
  } else {
    M5.Lcd.printf("[A] released\n");
  }
  delay(100);
}

void led_on() {
  digitalWrite (ledPin, LOW); //off
  delay(500); // wait for half a second or 500 milliseconds
  digitalWrite (ledPin, HIGH); // turn off the LED
//  delay(500); // wait for half a second or 500 milliseconds
}

void iotpush() {
  if ((WiFi.status() == WL_CONNECTED)) { //Check the current connection status
    HTTPClient http; 
    http.begin("http://1954b3fad743.ngrok.io/"); //Specify the URL
    int httpCode = http.GET();                                        //Make the request
 
    if (httpCode > 0) { //Check for the returning code
 
        String payload = http.getString();
        Serial.println(httpCode);
        Serial.println(payload);
      }
 
    else {
      Serial.println("Error on HTTP request");
    }
 
    http.end(); //Free the resources
  }
}
