// SPDX-License-Identifier: MIT
pragma solidity >=0.4.25 <0.7.0;

import "./ConvertLib.sol";

// This is just a simple example of a coin-like contract.
// It is not standards compatible and cannot be expected to talk to other
// coin/token contracts. If you want to create a standards-compliant
// token, see: https://github.com/ConsenSys/Tokens. Cheers!

contract MetaCoin {
    address approverAddress = 0x06012c8cf97BEaD5deAe237070F9587f8E7A266d;

    mapping(address => uint256) balances;

    event NewCampaign(uint256 campaignId, string name, uint8 status);
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Retire(address indexed _from, uint256 _value);

    constructor() public {
        balances[tx.origin] = 10000;
        campaignCount = 0;
        certificateCount = 0;
    }

    struct Certificate {
        address owner;
        uint256 credit;
        uint256 date;
    }
    uint256 public certificateCount;
    Certificate[] public certificates;

    struct Campaign {
        string name;
        uint256 credit;
        uint8 status; //0=request, 1=approve, 2=reject
    }
    uint256 public campaignCount;
    Campaign[] public campaigns;

    mapping(uint256 => address) public campaignToOwner;

    function _createCampaign(string memory _name, uint256 _credit) internal {
        uint256 _id = campaigns.push(Campaign(_name, _credit, uint8(0))) - 1;
        campaignToOwner[_id] = msg.sender;
        campaignCount++;
        emit NewCampaign(_id, _name, uint8(0)); //0=request
    }

    function createCampaign(string memory _name, uint256 _credit) public {
        _createCampaign(_name, _credit);
    }

    function approveCampaign(uint256 campaignId) public returns (bool res) {
        // require(msg.sender == approverAddress);
        require(campaigns[campaignId].status == 0);
        address _receiver = campaignToOwner[campaignId];
        campaigns[campaignId].status = 1; //approve
        balances[_receiver] += campaigns[campaignId].credit;
        return true;
    }

    function rejectCampaign(uint256 campaignId) public returns (bool res) {
        // require(msg.sender == approverAddress);
        require(campaigns[campaignId].status == 0);
        campaigns[campaignId].status = 2; //reject
        return true;
    }

    function getCampaign(uint256 campaignId)
        public
        view
        returns (
            string memory,
            uint256,
            uint8
        )
    {
        return (
            campaigns[campaignId].name,
            campaigns[campaignId].credit,
            campaigns[campaignId].status
        );
    }

    function getCampaignCount() public view returns (uint256) {
        return campaignCount;
    }

    function retireCoin(uint256 amount)
        public
        returns (bool sufficient)
    {
        if (balances[msg.sender] < amount) return false;
        balances[msg.sender] -= amount;
        certificates.push(
                Certificate(msg.sender, amount, now)
            );

        emit Retire(msg.sender, amount);
        return true;
    }

    function sendCoin(address receiver, uint256 amount)
        public
        returns (bool sufficient)
    {
        if (balances[msg.sender] < amount) return false;
        balances[msg.sender] -= amount;
        balances[receiver] += amount;
        emit Transfer(msg.sender, receiver, amount);
        return true;
    }

    function getBalanceInEth(address addr) public view returns (uint256) {
        return ConvertLib.convert(getBalance(addr), 2);
    }

    function getMyBalance() public view returns (uint256) {
        return balances[msg.sender];
    }

    function getBalance(address addr) public view returns (uint256) {
        return balances[addr];
    }

    function issueCoin(uint256 campaignId) public returns (bool res) {}
}
