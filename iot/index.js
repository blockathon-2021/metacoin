const express = require("express");
const app = express();
const port = 8889;
const { exec } = require("child_process");


app.get("/", (req, res) => {
  res.end("1");
  addCredit();
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

function addCredit() {
  const _cmd = "truffle exec exec_iot.js";
  exec(_cmd, (error, stdout, stderr) => {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
});
}
