var MetaCoin = artifacts.require("../contacts/MetaCoin.sol");

module.exports = function (callback) {
  main()
    .then(() => callback())
    .catch(err => {
      console.log(err);
      callback(err);
    });
};

async function main() {
  console.log("main");
  // const newtworkType = await web3.eth.net.getNetworkType();
  // const networkId = await web3.eth.net.getId();
  // console.log("network type:"+newtworkType);
  // console.log("network id:"+networkId);
  let instance = await MetaCoin.deployed();
  let accounts = await web3.eth.getAccounts();
  // console.log(instance);

  const receiver = accounts[1];
  let res = await instance.sendCoin(receiver, 1);
  let balance = await instance.getBalance(receiver);
  console.log(balance);
}
